﻿namespace SquaredPeople
{
	partial class FrmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
			this.pictureBox = new System.Windows.Forms.PictureBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.rbCross1 = new System.Windows.Forms.RadioButton();
			this.rbCross0 = new System.Windows.Forms.RadioButton();
			this.trackBar1 = new System.Windows.Forms.TrackBar();
			this.lBulletSize = new System.Windows.Forms.Label();
			this.rbDraw = new System.Windows.Forms.RadioButton();
			this.label1 = new System.Windows.Forms.Label();
			this.tMetri = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.lPeoples = new System.Windows.Forms.Label();
			this.lPM = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.lSQM = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox
			// 
			this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox.Location = new System.Drawing.Point(4, 65);
			this.pictureBox.Margin = new System.Windows.Forms.Padding(4);
			this.pictureBox.Name = "pictureBox";
			this.pictureBox.Size = new System.Drawing.Size(1399, 466);
			this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox.TabIndex = 0;
			this.pictureBox.TabStop = false;
			this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
			this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.pictureBox, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.77686F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1407, 535);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.AutoSize = true;
			this.tableLayoutPanel2.ColumnCount = 13;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.Controls.Add(this.rbCross1, 2, 0);
			this.tableLayoutPanel2.Controls.Add(this.rbCross0, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.trackBar1, 3, 0);
			this.tableLayoutPanel2.Controls.Add(this.lBulletSize, 4, 0);
			this.tableLayoutPanel2.Controls.Add(this.rbDraw, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.label1, 5, 0);
			this.tableLayoutPanel2.Controls.Add(this.tMetri, 6, 0);
			this.tableLayoutPanel2.Controls.Add(this.label2, 7, 0);
			this.tableLayoutPanel2.Controls.Add(this.label3, 11, 0);
			this.tableLayoutPanel2.Controls.Add(this.lPeoples, 12, 0);
			this.tableLayoutPanel2.Controls.Add(this.lPM, 8, 0);
			this.tableLayoutPanel2.Controls.Add(this.label4, 9, 0);
			this.tableLayoutPanel2.Controls.Add(this.lSQM, 10, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 4);
			this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(1399, 53);
			this.tableLayoutPanel2.TabIndex = 1;
			// 
			// rbCross1
			// 
			this.rbCross1.AutoSize = true;
			this.rbCross1.Location = new System.Drawing.Point(278, 4);
			this.rbCross1.Margin = new System.Windows.Forms.Padding(4);
			this.rbCross1.Name = "rbCross1";
			this.rbCross1.Size = new System.Drawing.Size(78, 20);
			this.rbCross1.TabIndex = 8;
			this.rbCross1.Text = "Cross 1";
			this.rbCross1.UseVisualStyleBackColor = true;
			// 
			// rbCross0
			// 
			this.rbCross0.AutoSize = true;
			this.rbCross0.Location = new System.Drawing.Point(192, 4);
			this.rbCross0.Margin = new System.Windows.Forms.Padding(4);
			this.rbCross0.Name = "rbCross0";
			this.rbCross0.Size = new System.Drawing.Size(78, 20);
			this.rbCross0.TabIndex = 7;
			this.rbCross0.Text = "Cross 0";
			this.rbCross0.UseVisualStyleBackColor = true;
			// 
			// trackBar1
			// 
			this.trackBar1.Dock = System.Windows.Forms.DockStyle.Top;
			this.trackBar1.Location = new System.Drawing.Point(364, 4);
			this.trackBar1.Margin = new System.Windows.Forms.Padding(4);
			this.trackBar1.Maximum = 100;
			this.trackBar1.Minimum = 1;
			this.trackBar1.Name = "trackBar1";
			this.trackBar1.Size = new System.Drawing.Size(342, 45);
			this.trackBar1.TabIndex = 4;
			this.trackBar1.Value = 50;
			this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
			// 
			// lBulletSize
			// 
			this.lBulletSize.AutoSize = true;
			this.lBulletSize.Location = new System.Drawing.Point(714, 0);
			this.lBulletSize.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lBulletSize.Name = "lBulletSize";
			this.lBulletSize.Size = new System.Drawing.Size(24, 16);
			this.lBulletSize.TabIndex = 5;
			this.lBulletSize.Text = "50";
			// 
			// rbDraw
			// 
			this.rbDraw.AutoSize = true;
			this.rbDraw.Checked = true;
			this.rbDraw.Location = new System.Drawing.Point(4, 4);
			this.rbDraw.Margin = new System.Windows.Forms.Padding(4);
			this.rbDraw.Name = "rbDraw";
			this.rbDraw.Size = new System.Drawing.Size(180, 20);
			this.rbDraw.TabIndex = 6;
			this.rbDraw.TabStop = true;
			this.rbDraw.Text = "Draw (CTRL for erase)";
			this.rbDraw.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(746, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(176, 16);
			this.label1.TabIndex = 9;
			this.label1.Text = "Meters between crosses";
			// 
			// tMetri
			// 
			this.tMetri.Location = new System.Drawing.Point(930, 4);
			this.tMetri.Margin = new System.Windows.Forms.Padding(4);
			this.tMetri.Name = "tMetri";
			this.tMetri.Size = new System.Drawing.Size(84, 22);
			this.tMetri.TabIndex = 10;
			this.tMetri.Text = "100";
			this.tMetri.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(1022, 0);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(86, 16);
			this.label2.TabIndex = 11;
			this.label2.Text = "Pixel/Meter";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(1305, 0);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(66, 16);
			this.label3.TabIndex = 14;
			this.label3.Text = "Peoples";
			// 
			// lPeoples
			// 
			this.lPeoples.AutoSize = true;
			this.lPeoples.Location = new System.Drawing.Point(1379, 0);
			this.lPeoples.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lPeoples.Name = "lPeoples";
			this.lPeoples.Size = new System.Drawing.Size(16, 16);
			this.lPeoples.TabIndex = 15;
			this.lPeoples.Text = "0";
			// 
			// lPM
			// 
			this.lPM.AutoSize = true;
			this.lPM.Location = new System.Drawing.Point(1116, 0);
			this.lPM.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lPM.Name = "lPM";
			this.lPM.Size = new System.Drawing.Size(40, 16);
			this.lPM.TabIndex = 12;
			this.lPM.Text = "NAN";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(1164, 0);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(109, 16);
			this.label4.TabIndex = 16;
			this.label4.Text = "Square Meters";
			// 
			// lSQM
			// 
			this.lSQM.AutoSize = true;
			this.lSQM.Location = new System.Drawing.Point(1281, 0);
			this.lSQM.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lSQM.Name = "lSQM";
			this.lSQM.Size = new System.Drawing.Size(16, 16);
			this.lSQM.TabIndex = 17;
			this.lSQM.Text = "0";
			// 
			// FrmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1407, 535);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "FrmMain";
			this.Text = "Square Map";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.FrmMain_Load);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmMain_KeyDown);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FrmMain_KeyUp);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBox;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TrackBar trackBar1;
		private System.Windows.Forms.Label lBulletSize;
		private System.Windows.Forms.RadioButton rbCross1;
		private System.Windows.Forms.RadioButton rbCross0;
		private System.Windows.Forms.RadioButton rbDraw;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tMetri;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lPM;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lPeoples;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lSQM;
	}
}

