﻿

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using AForge.Imaging;
using AForge.Imaging.Filters;

namespace SquaredPeople
{
	public partial class FrmMain : Form
	{
		private Bitmap _backGround;
		private Bitmap _overlay;
		private bool _ctrl;
		private PointF _cross0 = new PointF( 1143, 687 );
		private PointF _cross1 = new PointF( 1267, 686 );
		private double _pxm;
		private const string IMG_FILE = @"square.png";

		public FrmMain()
		{
			InitializeComponent();
		}

		private void FrmMain_Load( object sender, EventArgs e )
		{
			_backGround = new Bitmap( IMG_FILE );

			_overlay = new Bitmap( _backGround.Width, _backGround.Height, PixelFormat.Format24bppRgb );

			UpdateView();
		}

		private DateTime _nextUpdate;

		private void UpdateView()
		{
			if( DateTime.Now < _nextUpdate )
				return;

			_nextUpdate = DateTime.Now.AddSeconds( 1 );

			var filter = new Subtract( _overlay );

			var resultImage = filter.Apply( _backGround );

			using( var grf = Graphics.FromImage( resultImage ) )
			{
				grf.DrawLine( Pens.Red, _cross0.X - 5, _cross0.Y, _cross0.X + 5, _cross0.Y );
				grf.DrawLine( Pens.Red, _cross0.X, _cross0.Y - 5, _cross0.X, _cross0.Y + 5 );

				grf.DrawLine( Pens.Red, _cross1.X - 5, _cross1.Y, _cross1.X + 5, _cross1.Y );
				grf.DrawLine( Pens.Red, _cross1.X, _cross1.Y - 5, _cross1.X, _cross1.Y + 5 );
			}

			var stat = new ImageStatistics( _overlay );

			var sqm = stat.PixelsCountWithoutBlack / _pxm / _pxm;

			lSQM.Text = sqm.ToString( "N0" );

			lPeoples.Text = ( sqm * 4 ).ToString( "N0" );

			pictureBox.Image = resultImage;
		}

		private void pictureBox_MouseMove( object sender, MouseEventArgs e )
		{
			if( !rbDraw.Checked )
				return;

			//lX.Text = e.Location.X.ToString() ;
			//lY.Text = e.Location.Y.ToString() ;

			if( e.Button == MouseButtons.Left )
			{
				using( var grf = Graphics.FromImage( _overlay ) )
				{
					var point = FromMouse( e.Location );

					//lX.Text = e.Location.X.ToString() + " - " + point.X;
					//lY.Text = e.Location.Y.ToString() + " - " + point.Y;

					float SIDE = trackBar1.Value;

					var brsh = _ctrl ? Brushes.Black : Brushes.White;

					grf.FillEllipse( brsh, new RectangleF( point.X - SIDE / 2, point.Y - SIDE / 2, SIDE, SIDE ) );
				}
			}

			UpdateView();
		}

		private PointF FromMouse( Point location )
		{
			var ratioI = 1.0f * _overlay.Height / _overlay.Width;
			var ratioP = 1.0f * pictureBox.Height / pictureBox.Width;

			var ratioIP = ratioI / ratioP;

			var size = pictureBox.Size;

			if( ratioIP < 1 )
			{
				size = new Size( pictureBox.Width, (int) ( pictureBox.Height * ratioIP ) );

				var il = ( pictureBox.Height - size.Height ) / 2;

				location = new Point( location.X, (int) ( location.Y - il ) );
			}
			else
			{
				size = new Size( (int) ( pictureBox.Width / ratioIP ), pictureBox.Height );

				var il = ( pictureBox.Width - size.Width ) / 2;

				location = new Point( (int) ( location.X - il ), location.Y );
			}

			if( location.X < 0 || location.Y < 0 )
				return new PointF();

			var x = 1.0f * location.X * _overlay.Width / size.Width;
			var y = 1.0f * location.Y * _overlay.Height / size.Height;

			return new PointF( x, y );
		}


		private void FrmMain_KeyDown( object sender, KeyEventArgs e )
		{
			_ctrl = e.Control;
		}

		private void FrmMain_KeyUp( object sender, KeyEventArgs e )
		{
			_ctrl = e.Control;
		}

		private void trackBar1_Scroll( object sender, EventArgs e )
		{
			lBulletSize.Text = trackBar1.Value.ToString();
		}

		private void pictureBox_MouseDown( object sender, MouseEventArgs e )
		{
			if( rbCross0.Checked )
				_cross0 = FromMouse( e.Location );
			else if( rbCross1.Checked )
				_cross1 = FromMouse( e.Location );

			var dst = Math.Sqrt( Math.Pow( ( _cross0.X - _cross1.X ), 2 ) + Math.Pow( ( _cross0.Y - _cross1.Y ), 2 ) );

			_pxm = ( dst / double.Parse( tMetri.Text ) );

			lPM.Text = _pxm.ToString( "N2" );

			UpdateView();
		}
	}
}